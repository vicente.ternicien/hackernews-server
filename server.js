console.log('server is running')

const express = require('express')
const app = express()
const mongoose = require('mongoose')
const port = 3000
const XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
const cors = require ('cors');

app.use(cors());
var datostemp;

//connect to DB
mongoose.connect('mongodb://localhost:27017',{useNewUrlParser: true, useUnifiedTopology: true},
() => console.log('connected to the DB'))

//Load the data
var ourRequest = new XMLHttpRequest()
ourRequest.open('GET', 'https://hn.algolia.com/api/v1/search_by_date?query=nodejs')
ourRequest.onload = dataload
ourRequest.send();

//Function to add an identifier, to know if the object as been deleted
function dataload(){
    var newsdata = JSON.parse(ourRequest.responseText);
    for (i = 0; i < newsdata.hits.length; i++) {
        if (newsdata.hits[i].isdeleted == 1){}

        else {newsdata.hits[i].isdeleted = 2;}
      } 
    console.log(newsdata);
    datostemp = newsdata;
}

//ROUTES
app.get('/products/:id', function (req, res, next) {
    res.json({msg: 'This is CORS-enabled for all origins!'})
  })

app.get('/', (req, res) => res.send("hi"))

app.get('/news', fetchdata)

function fetchdata (req, res){
    res.send(datostemp.hits);
}

app.listen(port, () => console.log(`app listening on port ${port}!`))

